# Working with Buckets, Folders, and Objects

After completing this section you will know how to get data into and out of S3
using the AWS web console. You will learn how to:

- Create your AWS Account

- Create a bucket and upload files to it

- Move and Copy files between buckets

- Delete files

- Add tags to your buckets and objects to classify them

- Add user defined meta-data to your objects
