# Hands-on Lab: Upload Files to Buckets Using the AWS Console

1. AWS Management Console > Services > S3 > Create New Bucket
2. Add bucket name. Note that bucket names must be **unique** across **all
   AWS** > Choose region > Create
3. Select bucket name > Upload an object > Add files > select file(s) to
   upload > Upload

## Updating the Contents of a File

Simply upload a file with the same file name. S3 will overwrite the previous
version.
