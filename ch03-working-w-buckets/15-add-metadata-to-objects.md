# Adding Metadata to Objects

Object metadata is descriptive data describing an object. It consists of
key-value pairs, and is returned as HTTP headers on objects. There are two types
of metadata:

- System metadata
- User-defined metadata

## System Metadata

**System metadata** is maintained by S3. There are two types of system metadata:

- Metdata where only Amazon S3 can modify the value. Example: Ojbect creation
  date
- Metadata that user can modify. Example: storage class, versioning, encryption
  attributes

## User Metadata

**User metadata** are custom key-value pairs you can add to your object. Names
must begin with `x-amz-meta`, and are case-insensitive.

## Metadata vs. Tags

- Both are key-value pairs
- Tag keys are case sensitive
- Metadata keys are case **insensitive**
- Tags are used for categorizing storage to analyze cost, or along with
  permissions for fine-grained access control
- Metadata properties are returned as HTTP headers (tags are not)

## Adding Metadata to Objects in the Console

1. Upload files to your S3 bucket > Properties > Metadata > Add metadata
2. You should now be able to view your metadata on the "Properties" tag for each
   affected file

You can also add metadata to your objects at a later time by selecting the
object > Actions > Edit metadata

3. Open the file(s) in a browser tab > Open developer tools > Network > Note
   that the `x-amz-meta` headers are present in the response headers

## Types of System Metdata Keys

| Key                               | Use                                                                                                                             | Example              |
| --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- | -------------------- |
| `Content-Type`                    |                                                                                                                                 |                      |
| `Cache-Control`                   | Allows browsers/CDNs to cache object file for a user-defined amount of time                                                     | `max-age=600`        |
| `Content-Disposition`             |                                                                                                                                 |                      |
| `Content-Encoding`                | Specify type of charset the file contains                                                                                       | `UTF-8`              |
| `Content-Language`                | Specify the language of the content                                                                                             | `en-us`              |
| `Expires`                         | Sets an expiration date for the content, after which browser/CDN will know to fetch a new copy of the object from S3            | `1900/01/01`         |
| `x-amz-website-redirect-location` | Used in conjunction w/ static website hosting. Redirects request for an object redirected to another bucket object, or any URL | `http://example.com` |
