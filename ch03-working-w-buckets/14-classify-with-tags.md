## Classifying Your Buckets and Objects with Tags

In this lesson we'll discuss **object tags**. Object tags are key-value pairs
used to categorize storage.

## Use Cases

- Use tags to allow users/groups to read/write objects w/ a tag
- Create lifecycle rules to move objects based on a tag to cheaper storage tiers
- Use tags as filters for anaylytics and metics
- Track costs based on tag (by project, client, dept. etc.)

## Tag Restrictions

- Max 10 tags on object
- Max 50 tags on bucket
- keys must be unique
- Tag key can contain 128 unicode chars
- Tag value can contain 256 unicode chars
- Keys/values are case-sensitive

## Adding Tags to Buckets and Files

You can add tags to a bucket during creation, or afterwards by selecting the
bucket > Properties > Tags > Edit Tags.

### Adding Tags to Files

1. Upload file(s) to your S3 bucket as desired > Select file > Properties >
   Tags > Add tag > Upload
