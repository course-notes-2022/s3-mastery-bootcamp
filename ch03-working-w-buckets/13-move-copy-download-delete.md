# Move, Copy, Download and Delete Files

In this lesson, we'll move, copy, download, and delete objects in S3 from the
Management Console.

## Copying Data from One Bucket to Another

In this section, we'll copy data from one bucket in region A to another bucket
in region B.

1. Create a new bucket in a **different** region from the one in the previous
   lesson.
2. In original bucket, select item(s) you wish to copy > Actions > Copy >
   Destination > Browse S3 > Choose new bucket > Choose Destination > Copy

## Moving Files from One Bucket to Another

1. Create a new bucket.

2. In original bucket, select desired object(s) > Actions > Move > Destination >
   Browse S3 > select S3 bucket you wish to move file(s) to > Move. Note that
   the original bucket is now empty.

## Downloading Files

1. Navigate into desired bucket > select desired file(s) > Download

**Note:** with the web console, you can only download 1 file at a time. If you
want to download **multiple files** you must do so with the **CLI**.
