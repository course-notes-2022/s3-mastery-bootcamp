# Protecting your S3 Data

In this section we'll discuss how to further protect your data from accidental
updates/deletes by:

- Enabling versioning and encryption form the console and CLI
- Require MFA for deletes
- Setup cross-region replication
