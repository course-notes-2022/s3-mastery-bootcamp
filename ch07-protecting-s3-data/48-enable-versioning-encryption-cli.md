# How to Enable Versioning and Encryption: CLI

1. `aws s3 mb {bucket-name}`: create new bucket

2. `aws s3api put-bucket-versioning --bucket {bucket-name} --versioning-configuration Status=Enabled`:
   Enable versioning on bucket `{bucket-name}`

3. `aws s3api get-bucket-versioning --bucket {bucket-name}`: Check bucket
   versioning status

4. `aws s3api put-bucket-versioning --bucket {bucket-name} --versioning-configuration Status=Suspended`:
   **Dis**able versioning on bucket `{bucket-name}`

5. `aws s3api put-bucket-encryption --bucket {bucket-name} --server-side-encryption-configuration '{"Rules":[{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}]}'`:
   Enable encryption on bucket `{bucket-name}`

6. `aws s3api get-bucket-encryption --bucket {bucket-name}`: Check bucket
   encryption status

7. `aws s3api delete-bucket-encryption --bucket {bucket-name}`: **Dis**able
   bucket encryption
