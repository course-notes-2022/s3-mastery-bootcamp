# How to Set Up Cross-Region Replication

**Cross-region Replication** enables automatic, async copying of objects across
buckets in different AWS Regions.

## Why Use Cross-region Replication?

- Meet compliance requirements when you need further geographcial separation
  between copies of your data

- Minimize latency by replication of objects closer to where your users use them

- Increase operational efficiency

- Copy objects to another AWS account under different ownership

## Enable Cross-region Replication

1. AWS management console > Services > S3

2. Create **source** bucket > Enable versioning (**must** for X-region
   replication) > accept defaults and create.

3. Create **destination** bucket with versioning enabled in a **different**
   region.

4. Add some objects in the source bucket as desired, with multiple versions (if
   desired).

5. S3 > choose src bucket > Management > Replication rules

6. Destination > Choose bucket in this account OR specify bucket in another
   account

7. IAM role > choose existing IAM role or enter IAM role ARN > Save > Replicate
   existing objects > Submit

Note that the **Priority** setting on the rule allows you to set the priority
(higher-numbers apply first)

## Important Notes

- Deleting **versions** from your source bucket will **not** delete them from
  your destination

- Deleting the **object** from your source bucket will **not** add the delete
  marker to your destination object
