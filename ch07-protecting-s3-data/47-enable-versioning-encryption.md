# How to Enable Versioning and Encryption

In this lecture we'll learn how to enable **versioning** and **encryption** on
our S3 buckets.

**Versioning** allows us to protect our data by allowing us to keep **multiple
versions** of the same objects.

**Encryption** allows us to maintain the privacy of our **data at rest** in our
S3 buckets.

## Enable Versioning and Encryption

1. AWS management console > Services > s3 > Bucket Versioning > enable > Default
   encryption > enable

Note that there are two `Encryption key type` options available:

- Amazon S3-managed keys: This is a service that Amazon manages your encryption
  keys **for you**
- AWS Key Management Service key: You manage your **own keys**. Some additional
  fees associated.

2. Upload a file to your bucket. Make changes to the file as many times as you
   like, saving the changes and uploading the **same file name** each time.

3. S3 > select bucket > select filename > Versions > Note that there are now
   multiple versions of the file in S3 bucket.

## Reverting to a Previous Version

Delete the current version to revert back to the previous version.

## Deleting a Versioned Object

Delete the object from the bucket. Note that the object no longer appears in the
`Objects` menu, **unless** you toggle the "Show versions" button **on**. The
versions of your deleted object now appear, along with a **delete marker** that
indicates the object has been deleted. If you want to **undelete** the object,
select the **delete marker**, and then delete the **delete marker**.
