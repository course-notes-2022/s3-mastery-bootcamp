# How to Require MFA to Delete Objects

In this lesson we'll learn how to lock down objects in a bucket so that they can
be deleted by the **root account only**, and **only with MFA enabled**.

Note that MFA-delete can be set up from the **CLI** only, and **only from the
root account**. Therefore, we need to **configure our CLI** to use our **root
account**:

1. `aws configure`: Add **Access Key ID** and **Secret Access Key** for your
   **root user**.

2. `aws s3api put-bucket-versioning --bucket {bucket-name} --versioning-configuration MFADelete=Enabled,Status=Enabled --mfa "{your-mfa-device-arn} {auth-code-from-device}"`:
   Create an S3 bucket and enable MFA-delete. To find MFA device ARN: AWS
   management console > select root user from dropdown > My Security
   credentials > MFA Authentication > copy ARN

3. Upload objects/versions to your bucket as desired.

4. Attempt to delete an object from the CLI:

   - `aws s3api delete-object --bucket {bucket-name} --key {object-key}`. List
     objects, and note that a delete marker has been created.

5. Attempt to delete a specific **object version** from the CLI:

   - `aws s3api delete-object --bucket {bucket-name} --key {object-key} --version-id {object-version-id}`.
     Note that MFA will be required for this request.

6. - `aws s3api delete-object --bucket {bucket-name} --key {object-key} --version-id {object-version-id} --mfa "{device-arn} {auth-code-from-device}"`:
     Delete an object with MFA authentication.
