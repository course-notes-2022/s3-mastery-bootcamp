# Course Overview

## Who is this Course For?

This course is for **Solutions Architects**, **Software engineers or
developers**, **Sys Admins** or anyone who wants an in-depth knowledge of Amazon
S3.

## Why Learn About S3?

- AWS skills are highly desired by employers
- Amazon S3 is one of Amazon's core cloud services
- Take advantage of low-cost, highly-available cloud storage for your own
  projects

## What You Need to Complete this Course

- An AWS account
- Web browser/internet acccess
- AWS CLI tools
- Familiarity with terminal/command prompt

## What Will You Learn in this Course?

- Fundamental Concepts
- S3 CRUD using the S3 web console
- How to manage your S3 data with the CLI
- How to control access to your data with security policies
- How to protect your data with
  - Versioning
  - Use cross-region replication for additional redundancy
  - Encryption
- Enable life cycle management to control costs
- Turn on logging to track access to your data
- Use events to trigger notifications
- Host a highlhy available static website
