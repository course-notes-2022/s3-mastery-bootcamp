# Create a Bucket and Upload a File

1. `aws s3 ls`: List all buckets in all regions for the AWS account

2. `aws s3 mb s3://{bucket-name} --region {region-name}`: Create bucket
   `bucket-name` in region `region-name`

3. `aws s3 cp {source-path} s3://{bucket-name}`: Copy file at `source-path` to
   s3 bucket `bucket-name`

4. `aws s3 ls s3://{bucket-name}`: List contents of S3 bucket {bucket-name}
