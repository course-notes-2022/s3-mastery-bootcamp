# Using the AWS CLI in S3

In this section we will:

- Install the aWS CLI
- Learn commands for working with S3:
  - High-level commands make common tasks such as uploading, moving, and
    deleting files easy
  - Low-level commands expose direct access to S3 API operations to perform
    advanced operation

We'll learn to:

- Create a bucket
- upload files
- delete files
- sync files
- Add tags and metadata to objects
