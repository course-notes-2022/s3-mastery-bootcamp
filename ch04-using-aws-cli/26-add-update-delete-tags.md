# Add, Update, and Delete Tags from Objects

1. `aws s3api list-objects --bucket {bucket-name}`: List all objects in bucket
   as a `JSON` document. This command is useful because we need the `Key`
   property of the object whose tags we wish to modify:

```json
{
  "Contents": [
    {
      "Key": "Contact.xml",
      "LastModified": "2022-12-06T14:25:16+00:00",
      "ETag": "\"5ac6c108c9f8f105d33ed9a0e6425c84\"",
      "Size": 120,
      "StorageClass": "STANDARD",
      "Owner": {
        "DisplayName": "disp-name",
        "ID": "65402e96e96822c9426b0c2fd9dc7787a1911fd761991c1d3c975a7fb6b962b8"
      }
    }
  ]
}
```

2. `aws s3api put-object-tagging --bucket {bucket-name} --key {obj-key} --tagging "TagSet=[{Key=color,Value=yellow}]"`

3. `aws s3api get-object-tagging --bucket {bucket-name} --key {object-key}`: Get
   tags for object identified by `object-key`

4. `aws s3api put-object-tagging --bucket {bucket-name} --key {obj-key} --tagging "TagSet=[{Key=color,Value=yellow},{Key=shape,Value=start}]"`:
   Update tags on object. **Note**: this command will **overwrite** all tags on
   the object

5. `aws s3api delete-object-tagging --bucket {bucket-name} --key {object-key}`:
   Delete all tags on the object
