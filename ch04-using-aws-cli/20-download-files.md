# Downloading Files from Amazon S3

1. `aws s3 ls s3://{bucket-name}`

2. `aws s3 cp s3://{bucket-name}/{file-name} {local-dest-path}`: Download the
   file `file-name` to `local-dest-path` on your local machine

3. 2. `aws s3 cp s3://{bucket-name} {dest-dir} --recursive`: Download files/dirs
      recursively to `dest-dir` on local machine
