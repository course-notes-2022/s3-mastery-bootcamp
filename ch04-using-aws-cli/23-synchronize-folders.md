# Synchronize Folders and Buckets

**Synchronizing** will only upload/overwrite files that **have changed between
locations**.

1. `aws s3 mb s3://{bucket-name} --region {region-name}`

2. `aws s3 sync {src-location} s3://{target-bucket}`

3. Make a change to any files in the `src-location`.

4. Re-run sync command from step 2. Note that **only the changed file(s)** are
   uploaded.

## Delete a Synced File

1. Delete a file from the source location.

2. Re-run the `sync` command. Note that the file **still exists in the S3
   bucket.**

3. Run the `sync` command with the `--delete` flag. Note that the file has now
   been deleted.

## Sync Buckets in Different Regions

1. `aws s3 sync s3://{src-bucket} s3://{dest-bucket}`
