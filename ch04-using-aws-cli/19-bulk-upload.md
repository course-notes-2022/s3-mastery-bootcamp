# Bulk-uploading All Files in a Folder to AWS S3

1. `aws s3 cp {source dir} s3://{bucket-name} --recursive`: Recursively copy all
   files/folders in `source-dir` to `bucket-name`

2. `aws s3 ls s3://{bucket-name} --recursive`: List all bucket contents
   recursively

## Upload From Local Folder to Subfolder in S3 Bucket

1. `aws s3 cp . s3://{bucket-name}/{subfolder-name} --recursive`
