# Add Metadata to Objects Using the Copy Command

1. `aws cp {src} s3://{dest-bucket-name} --recursive --metadata "customer=acme,author=foojenkins"`:
   Copy the contents of {src} to {dest}, adding **metadata** to the **copied
   objects**

2. `aws s3api head-object --bucket {bucket-name} --key {object-key}`: Returns
   the **metadata** of object identified by `object-key`

**Note that metadata can be added only when you upload or copy an object.**
