# Managing Bucket Tags with Low-Level Commands

1. `aws s3api put-bucket-tagging --bucket {bucket-name} --tagging "TagSet=[{Key=department,Value=sales}, {Key=customer,Value=acme}]"`

**Note the following when using LOW-LEVEL commands**:

- The `s3api` command indicates that we are using the low-level API
- The **bucket-name** is **not** prefixed with `s3://`
- The **double-quotes** around the `TagSet` option(s) are **mandatory**

2. `aws s3api get-bucket-tagging --bucket {bucket-name}`: View tags associated
   with bucket `bucket-name`

3. `aws s3api delete-bucket-tagging --bucket {bucket-name}`: Delete tags
   associated with bucket

4. `aws s3api put-bucket-tagging --bucket {bucket-name} --tagging "TagSet=[{Key=department,Value=operations}]"`:
   **UPDATE** the tag list using the same `put-bucket-tagging` command. **Note**
   that this will **overwrite** the current tag list!
