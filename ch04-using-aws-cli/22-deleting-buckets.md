# Deleting Buckets and Files with the CLI

1. `aws s3 ls`: List all buckets

2. `aws s3 rb s3://{bucket-name}`: Remove **empty** bucket (run with `--force`
   flag to remove full bucket)

## Remove an Item from a Bucket

1. `aws s3 rm s3://{bucket-name}/{file-path}`: Delete file at `file-path` from
   bucket

## Delete a Folder from a Bucket

1. `aws s3 rm s3://{bucket}/{file-path} --recursive
