# Using Filters to Include and Exclude Copied Files

**Filters** allow us to include or exclude files during copy, move, and sync
operations.

1. `aws s3 cp {src-file} s3://{dest-bucket} --recursive --exclude "*"`: Copy
   files from source to dest recursively, **excluding all files** (i.e. NO files
   copied)

2. `aws s3 cp {src-file} s3://{dest-bucket} --recursive --exclude "{string}"`:
   Copy all files excluding those that match `{string}`

   - `aws s3 cp {src-file} s3://{dest-bucket} --recursive --exclude "\*.docx"

3. `aws s3 cp {src} s3://{dest} --recursive --exclude "*" --include "*.docx"`:
   Copy **only** the files with a `.docx` extension.

   - `aws s3 cp {src} s3://{dest} --recursive --exclude "*" --include "*images*" --exclude "*.jpg"`

   - `aws s3 cp {src} s3://{dest} --recursive --exclude "*" --include "*images*.png"`
