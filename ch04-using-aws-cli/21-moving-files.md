# Moving Files from One Bucket to Another

1. `aws s3 ls`

2. `aws s3 ls s3://{bucket-name}`

3. `aws s3 mb s3://{copy-bucket-name} --region {region-name}`

4. `aws s3 cp s3://{src-bucket} s3://{dest-bucket} --recursive`: Copy
   files/folders from `src-bucket` to `dest-bucket`

5. `aws s3 mv s3://{src-bucket} s3://{dest-bucket} --recursive`: Moves files to
   `dest-bucket` and deletes them from `src-bucket`
