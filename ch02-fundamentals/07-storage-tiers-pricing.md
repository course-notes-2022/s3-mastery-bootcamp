# Storage Tiers and Pricing

Storage classes provide a way to select the most cost effective storage for your
data based on how frequently it is accessed and how quickly you need to retrieve
it.

## Storage Classes

1. Standard Storage: best when you need quick access, frequently

2. Standard Infrequent Access (IA): Use when you need data returned quickly, but
   not frequently accessed. Minimum 128K object size and min storage timeframe
   of 30 days

3. One-Zone Infrequent Access (IA): Use when you have another copy or data can
   be re-created. Cheaper, but data is only replicated in **one** AZ

4. Glacier: used for long-term archive and backup. Minimum timeframe of 90 days,
   can take minutes/hours to retrieve.

## Durability and Availability

Most expensive per GB stored <===> Least Expensive per GB stored

|                           | Standard      | Standard-IA   | One Zone-IA   | Glacier       |
| ------------------------- | ------------- | ------------- | ------------- | ------------- |
| Designed for durability   | 99.999999999% | 99.999999999% | 99.999999999% | 99.999999999% |
| Designed for Availability | 99.99%        | 99.9%         | 99.5%         | N/A           |

Least expensive per Request <===> Most expensive per Request

## S3 Pricing

- Data transfer **IN** is **free**
- Data transer **OUT** is **not free**
- Pay for **requests** (per 1000)
- Pay for amount of data stored
- Per-GB pricing generally goes **down** the more data you store
- Pricing differs across regions
