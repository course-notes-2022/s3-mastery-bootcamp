# Shared Responsibility Model and Security

The **Shared Responsibility Model** means that you as the **customer** and
**AWS** share responsibility for different parts of security.

The **customer** is responsible for "security in the cloud":

- setting appropriate access controls for your objects and buckets
- Control who accesses the AWS console or APIs

**AWS** is repsonsible for **security of the cloud**:

- Physical security of data centers and global infrastructure

## How Secure is my Data?

AWS infrastructure and services meet numerous compliance standards and
regulations. S3 supports SSL for encryption in transit, and optionally you can
enable encryption at rest. Your data does not leave the region you specify.
