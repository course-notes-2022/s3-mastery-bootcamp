# S3 Fundamental Concepts

- High-level concepts and terms that will be helpful
- Decision-making information:
  - How to choose a pricing tier
  - Compliance
  - Tools available to get data out of S3
