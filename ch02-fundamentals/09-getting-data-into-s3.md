# Getting Data Into and Out of S3

We can get data into/out of S3 via the **Management Console** and the **AWS
CLI**, as well as **AWS SDKs** for all popular programming languages.

You can also get data into/out of S3 via **AWS Import/Export**. This is a
service offered by Amazon in which you request a "job" in the Console, then
**physically ship your hard drive** to Amazon. They then import your data and
ship your drive back to you. This service is available for data **up to 16TB per
job**.

**AWS Snowball** is available for **lots of data** (50TB and 80TB models).
Amazon will ship a device to you, to which you copy your data. Data on the
device is encrypted. You then ship the device back to Amazon and they import
your data.

**AWS Snow Mobile** is available for **exabyte scale transfer service**. It is
essentially a mobile data center on a truck. Dedicated security personnel, with
video surveillance and GPS tracking.
