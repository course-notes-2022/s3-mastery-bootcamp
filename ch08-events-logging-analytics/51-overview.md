# Events, Logging, and Analytics

In this section, we'll:

- Setup email notifications when object events occur (upload, modified, deleted
  etc)

- Turn on server access logging to record all requests on a bucket

- Enable object-level logging with CloudTrail for centralized and granular
  logging

- Understand analysis tools (Analytics, Metrics, and Inventory)
