# How to Receive a Notification When an Object is Uploaded

1. Create bucket in S3 and accept all defaults > Properties > Event
   notifications > Create event notification

2. Event types > add desired event(s) > Destination > select destination to
   publish the event > SNS topic

3. Services > Simple Notification Service > Create new SNS topic > add topic
   details > Create Topic

_Note: The video seems out of date at this point. The AWS Management Console no
longer seems to allow creating a subscription with the HTTP protocol._
