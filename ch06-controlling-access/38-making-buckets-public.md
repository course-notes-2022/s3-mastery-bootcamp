# Making Buckets or Objects Public with ACLs

1. AWS Management Console > Services > S3 > select bucket > Permissions > Select
   from the following options:
   <blockquote>

   - Block _all_ public access: Turning on this setting is the same as turning
     on all 4 of the following

   - Block public access to buckets and objects granted through new access
     control lists (ACLs): S3 will block public access permissions applied to
     newly added buckets or objects, and prevent the creation of new public
     access ACLs for existing buckets and objects. This setting doesn’t change
     any existing permissions that allow public access to S3 resources using
     ACLs.

   - Block public access to buckets and objects granted through any access
     control lists (ACLs): S3 will ignore all ACLs that grant public access to
     buckets and objects.

   - Block public access to buckets and objects granted through new public
     bucket or access point policies: S3 will block new bucket and access point
     policies that grant public access to buckets and objects. This setting
     doesn't change any existing policies that allow public access to S3
     resources.

   - Block public and cross-account access to buckets and objects through any
   public bucket or access point policies: S3 will ignore public and
   cross-account access for buckets or access points with policies that grant
   public access to
   </blockquote>

   Ensure 1 - 3 are **unchecked** to grant public access through **ACLs**.

2. Access Control List > Edit > Everyone > check "List"

3. Select a file in the S3 bucket > Copy URL

4. Attempt to access the URL in an **incognito browser window**. Note that you
   do not yet have access to the **object**.

5. Attempt to access the **bucket URL**. Note that you do have access to the
   **bucket**.

6. Select object > Permissions > Edit > Access control list > Everyone > Read >
   Note that you now have access to the object via its URL in the browser

**Note that in most cases**, you probably will **not** want to make your S3
objects/buckets **publicly accessible**, though there are some valid use cases.
