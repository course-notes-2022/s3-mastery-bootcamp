# Allowing Access to a Specific Folder/Prefix or a Single Object

In this lecture, we'll set up the developers' policy, group, and user to give
developers access to **only** the development folder.

1. Create an S3 policy with the following permissions: ListAllMyBuckets,
   ListBucket, Getobject, PutObject on **specific** resource identified by the
   **ARN** of the first bucket you created in the **previous lesson**, on the
   **objects** with the `development` prefix:

   - `arn:aws:s3:::{bucket-name}/development/*`: Insert this value in the
     "Specify ARN for object" field when creating the policy

2. Review and Create policy

3. Create new `developers` IAM group and attach the new policy > Create group

4. Create a new developer user and add to `developers` group. Give Console (and
   programmatic access).

5. Navigate to S3 as developer user. Note that bucket access is restricted to
   only `bucket-name`, and files with the `development` prefix.

6. IAM > Choose policies > choose developer policy > Add additional
   permissions > S3 > GetObject > add `task_assignments.txt` object > Save

You should now be able to **download** the `task_assignments.txt` file as the
developer user.
