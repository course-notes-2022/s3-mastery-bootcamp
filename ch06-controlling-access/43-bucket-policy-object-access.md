# Using a Bucket Policy to Grant Access to Objects in a Bucket

In this lesson, we'll grant object access to a specific **IAM user** for a
bucket.

1. AWS Management Console > Services > S3 > select bucket > Permissions > Bucket
   Policy > paste into Bucket Policy Editor:

```json
{
  "Version": "2012-10-17",
  "Id": "Policy1548953865021",
  "Statement": [
    {
      "Sid": "Stmt1548953840321",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::<account_id>:user/developer-1"
      },
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::demo-images-bucket"
    },
    {
      "Sid": "Stmt1548953873214",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::<account_id>:user/developer-1"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::demo-images-bucket/*"
    }
  ]
}
```

**NOTE**: Replace `<account_id>` with **your** AWS Account ID, and
``developer-1` with the **user name** of the principal.

Save new bucket policy.

2. Sign in to the Console as the **IAM principal**, and attempt to download a
   file from the bucket. Note that you should be able to download the file
   successfully.
