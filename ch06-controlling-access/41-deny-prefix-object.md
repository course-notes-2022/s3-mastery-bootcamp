# Deny Access to Objects with a Prefix

In this lecture, we'll implement a **deny** permission to disallow project
managers from downloading files in the `developer` folder.

1. AWS Management Console > Services > IAM > Policies > select project manager
   policy > Permissions > Edit permissions > Add permissions > select S3

2. Switch to deny permissions > Access level > Read > select `GetObject` >
   Resources > Specific > Add object ARN for `development` folder > Object
   name > add `*` to deny access to **all** objects in the folder

3. Review policy > Save changes

4. Login as project manager user. Attempt to access `development` bucket and
   download a file. Note that you should be **denied access** to the
   `development` folder objects.
