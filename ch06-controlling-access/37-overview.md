# Controlling Access to your Buckets and Objects

There are several controls that factor into securing your S3 bucket:

- ACLs
- Public Access Settings
- Bucket Policies
- IAM Users, Groups and Policies

## ACLs

**Access Control Lists** grant high-level permissions to other AWS accounts, or
quickly make a bucket/file public. You can set `LIST`, `READ`, `WRITE`, and
`PERMISSION TO CHANGE PERMISSIONS` via ACLs.

## Bucket Policies

**Bucket Policies** allow/deny specific principals to perform certain actions on
specific resources. It is a `JSON` document attached to a bucket. We can use
bucket policies to grant very granular permissions on a bucket, object, or tag.

## IAM Users, Groups, and Policies

An IAM **policy** is a JSON docuemnt that allows/denies a user, group, or
service to perform specific actions on resources.

**Users** can be assigned to **groups** to make managing security policies
easier.

## Public Access Settings

**Public Access Settings** allow you to quickly/easily turn off public access to
your buckets and objects, or prevent new public permissions being granted on
them.

## What if I Use IAM Policies, Bucket Policies, AND ACLs?

In AWS, security is designed around the principle of **least privilege**. Thus,
**all S3 buckets and objects** are **private by default**.

If permissions are not specified **anywhere**, it is **DENIED** by default.

If one policy **allows** an action and another **denies**, then the action is
**DENIED**.

A **deny** present in any policy **supersedes any allow** permission for the
action and resource in question.
