# Example 1: Using IAM Policy to Specify Permissions for User Group

Imagine we have a team of developers and PMs working on a project. We want to
have all project files in one bucket. PMs will have access to all files.
Developers can read/write all files in a **development** folder. Developers also
have **read-only** access to a list of task assignments:

![project structure](./screenshots/project-structure.png)

1. Create bucket > add bucket name > accept defaults > Create bucket

2. Upload files in `course/downloads/permissions-demo/development` directory to
   bucket.

3. Create a second bucket. Upload `top-secret.txt`.

4. Setup IAM group for PMs: > IAM > Policies > Create new policy > Service >
   S3 > Add the following permissions: List Bucket, List All Buckets, GetObject,
   DeleteObject, PutObject

5. Resources > Specific > bucket > Add ARN > Copy/paste ARN of bucket 1 >
   object > add ARN > Copy/paste ARN of bucket 1 and select "Any" for objects >
   Create policy

6. Attach new policy to new IAM user group

7. Users > Add new user > Create a new user and add to new project managers' IAM
   group

8. Sign in with new PM user and access S3 bucket. Verify that bucket access is
   allowed/restricted as expected.
