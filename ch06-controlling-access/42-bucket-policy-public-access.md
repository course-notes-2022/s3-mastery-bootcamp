# Using a Bucket Policy to Grant Public Access

In this lecture we'll learn how to implement a **bucket policy** to make objects
in a bucket public. So far, we've learned how to use ACLs or IAM roles to
configure access to our buckets.

First, we must ensure that **public access settings** for the **account** are
**not** blocked:

1. AWS Management Console > Services > S3 > Block Public Access settings for
   this account > ensure that "Block _all_ public access" is turned **off**.

**Note**: AWS recommends that you turn _on_ this setting. Investigate ways to
enable access using ACLs, IAM roles/policies, or bucket policies with this
setting **on**.

2. Create a new bucket for this demo. Accept all defaults.

3. S3 > Select bucket > Permissions > Public access settings > Edit Block public
   access (bucket settings) > **Un-check**
   `Block public access to buckets and objects granted through new public bucket or access point policies`
   and
   `Block public and cross-account access to buckets and objects through any public bucket or access point policies` >
   Save changes

4. Permissions > Bucket policy > Policy Generator > Select Type of Policy > S3 >
   Effect > Allow > Principal > `*` > AWS Service > S3 > Actions > GetObject >
   ARN > insert object ARNs as comma-separated list > Add statement > Generate
   Policy

5. Copy policy > paste in Policy Generator tool > Save changes

You should now be able to access the bucket's objects from an **incognito**
browser window.

## Remove Public Access

1. S3 > select bucket > Permissions > Bucket policy > Delete

Note that the bucket is no longer `Public`, and the objects inside are no longer
accessible from an incognito browser window.
