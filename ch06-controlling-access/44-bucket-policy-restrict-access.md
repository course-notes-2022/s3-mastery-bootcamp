# Using a Bucket Policy to Restrict Access Based on an Object Tag

1. AWS Management Console > Services > S3 > Select bucket > select desired
   object in bucket > Properties > Tags > Edit Tags > Add tag > key =
   "finalized", value = "true"

2. Add the "finalized" tag to all objects in the bucket. Vary the value between
   true and false.

3. Modify the bucket policy attached to the bucket from the **previous** lesson
   with the `Condition` attribute:

```json
{
  "Version": "2012-10-17",
  "Id": "Policy1548953865021",
  "Statement": [
    {
      "Sid": "Stmt1548953840321",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::<account_id>:user/developer-1"
      },
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::demo-images-bucket"
    },
    {
      "Sid": "Stmt1548953873214",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::<account_id>:user/developer-1"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::demo-images-bucket/*",
      "Condition": {
        "StringEquals": { "s3:ExistingObjectTag/finalized": "true" }
      }
    }
  ]
}
```

4. Sign into an incognito window as your developer principal. Attempt to access
   and download finalized and non-finalized objects.
