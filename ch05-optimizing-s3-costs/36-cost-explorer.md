# S3 Cost Analysis Using Cost Explorer

Use **Cost Explorer** to track costs by **bucket tags**:

1. AWS Management Console > Services > Billing > Cost Allocation Tags > Activate
   tag for your S3 bucket(s) if inactive. Note that the activation can take some
   time to become effective.

2. AWS Cost Management > Cost Explorer > Filters > select S3 (or desired
   service) to display
