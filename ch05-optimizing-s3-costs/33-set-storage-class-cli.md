# Setting Object Storage Classes from the CLI

1. `aws s3 cp {src} s3://{dest-bucket} --recursive --storage-class STANDARD_IA`:
   Copy files from `src` to detination bucket and set storage class on objects
   to Standard IA

2. `aws s3 cp s3://{bucket-name}/{file-name} s3://{bucket-name}/{file-name} --storage-class {STORAGE-CLASSNAME}`:
   Update the storage class name on file `file-name` in bucket `bucket-name`

3. `aws s3 cp s3://{bucket-name}/{folder-name} s3://{bucket-name}/{folder-name} --recursive --storage-class {STORAGE-CLASSNAME}`:
   Update storage class on multiple files with `folder-name` prefix
