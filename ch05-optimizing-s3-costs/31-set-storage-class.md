# Setting Object Storage Classes from the AWS Console

1. Create a new S3 bucket (if necessary). Upload files from the Management
   Console, or copy them from the CLI terminal.

2. In the management console: Services > S3 > select bucket > select desired
   file(s)/folder(s) > Actions > Edit storage class > select storage class >
   Change storage class

   Note that when selecting a folder, AWS will apply the changes to **all
   objects in the folder**.
