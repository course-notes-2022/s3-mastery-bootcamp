# Retrieving Objects from Glacier

You cannot download or copy objects that are stored in Glacier. You must first
**initiate a restore** from Glacier:

1. AWS Management Console > Services > S3 > select object in Glacier > Actions >
   Initiate restore

2. Number of days that the restored copy is available > insert number of days >
   Retrieval tier > select retrieval tier > Initiate restore

Note that the object will be deleted after the number of days you select.
