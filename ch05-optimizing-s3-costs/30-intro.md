# Optimizing Amazon S3 Costs

In this chapter, we'll discuss managing S3 costs.

With S3, you get virtually unlimited storage for pennies on the dollar.

## What You Pay for with S3

- Per GB stored per month (approx. $0.023 to $0.004)
- Requests (GET, PUT, POST, LIST) (approx $0.005 5o $0.001 per 1000 requests)
- Data transfer OUT (approx $0.00 to $0.09 per GB)
- Data transfer IN is free
- Get 5GB free standard storage, 10,000 GET requests, 2000 PUTs, and 15GB Data
  transfer out for first year

## Expenses by Tier

Most expensive per GB stored <===> Least Expensive per GB stored

|                           | Standard      | Standard-IA   | One Zone-IA   | Glacier       |
| ------------------------- | ------------- | ------------- | ------------- | ------------- |
| Designed for durability   | 99.999999999% | 99.999999999% | 99.999999999% | 99.999999999% |
| Designed for Availability | 99.99%        | 99.9%         | 99.5%         | N/A           |

Least expensive per Request <===> Most expensive per Request

## What We'll Learn

- How to set storage classes on objects
- How to create lifecycle configurations to automatically change storage classes
  based on rules you set up
- How to get data out of Glacier
- Use CostExplorer to analyze storage costs
